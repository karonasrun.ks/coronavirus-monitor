<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use PragmaRX\Countries\Package\Countries;

class IndexController extends Controller
{
    /**
     * Index function
     *
     * @return void
     */
    public function index()
    {
        $datas = Http::get('https://corona.lmao.ninja/v2/countries')->throw()->json();
        return view('monitor',['datas'=>$datas]);
    }

    public function country()
    {
        # code...
        $countries = Countries::all();
        $countries->toJson();
        return view('country', compact('countries'));
    }

    /**
     * Search Country function
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function search(Request $request)
    {
        # code...
        $datas = Http::get('https://coronavirus-19-api.herokuapp.com/countries/{countryName}');
    }
}
