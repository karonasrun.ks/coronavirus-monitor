@extends('index')

@section('content')
    <h2 class="text-center">Countries Lists</h2>
    @if($countries->count())
        @foreach($countries as $country)
            <span style="padding:5px"> {!!  $country->flag['flag-icon'] !!} {!!  $country->name->common !!} </span>
        @endforeach
    @endif
@endsection
