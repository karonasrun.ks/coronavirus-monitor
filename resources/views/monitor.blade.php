@extends('index')

@section('css')
    <style>
        #marker {
            background-image: url('https://i.imgur.com/FR57JDJ.gif');
            background-size: cover;
            width: 35px;
            height: 35px;
            border-radius: 50%;
            cursor: pointer;
        }

        .mapboxgl-canvas {
            width: 100% !important;
        }

        .mapboxgl-map {
            width: 100% !important;
            height: 250px;
        }

        .mapboxgl-popup {
            max-width: 200px;
        }

        .mapboxgl-popup-content {
            color: #dc0000 !important;
            border-radius: 5px !important;
            width: 200px !important;
        }

        .mapboxgl-popup-close-button {
            border-radius: 10px !important;
        }

        .mapboxgl-popup-close-button:hover,
        .mapboxgl-popup-close-button:after {
            background: transparent !important;
        }

    </style>
@endsection
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">CoronaVirus Monitor</h1>
        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-6 col-12">
            <div class="input-group">
                <input type="text" class="form-control bg-light border-0 small" name="q" id="q" placeholder="Country..."
                    aria-label="Search" aria-describedby="basic-addon2">
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        @foreach($datas as $key => $data)
            <div class="col-xl-3 col-md-4 mb-4 data toggle" data-toggle="modal" data-target="#logoutModal"
                data-country="{{ $data['country'] }}" data-cases="{{ $data['cases'] }}"
                data-todayCases="{{ $data['todayCases'] }}" data-deaths="{{ $data['deaths'] }}"
                data-recovered="{{ $data['recovered'] }}" data-active="{{ $data['active'] }}"
                data-critical="{{ $data['critical'] }}" data-lat="{{ $data['countryInfo']['lat'] }}" data-long="{{ $data['countryInfo']['long'] }}">
                <div class="card border-left-primary shadow h-100">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-lg font-weight-bold text-primary text-uppercase mb-1">
                                    {{ $data['country'] }}</div>
                                <div class="h6 mb-0 font-weight-bold text-danger">Case: {{ $data['cases'] }}</div>
                                <div class="h6 mb-0 font-weight-bold text-gray-800">Today: {{ $data['todayCases'] }}</div>
                            </div>
                            <div class="col-auto">
                                <img class="flag" src="{{ $data['countryInfo']['flag'] }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="country" class="text-bold"></p>
                    <p id="cases"></p>
                    <p class="text-primary" id="todayCases"></p>
                    <p class="text-danger" id="deaths"></p>
                    <p class="text-primary" id="recovered"></p>
                    <p class="text-warning" id="active"></p>
                    <p class="text-danger" id="critical"></p>
                    <br>
                    <div id="map"></div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('.toggle').click(function() {
                $('#exampleModalLabel').text("Information Detail of Country: " + this.getAttribute(
                    "data-country"));
                $('#country').text("Country: " + this.getAttribute("data-country"));
                $('#cases').text("Cases: " + this.getAttribute("data-cases"));
                $('#todayCases').text("TodayCases: " + this.getAttribute("data-todayCases"));
                $('#deaths').text("Deaths: " + this.getAttribute("data-deaths"));
                $('#recovered').text("Recovered: " + this.getAttribute("data-recovered"));
                $('#active').text("Active: " + this.getAttribute("data-active"));
                $('#critical').text("Critical: " + this.getAttribute("data-critical"));
                
                var monument = [this.getAttribute("data-long"),this.getAttribute("data-lat")];
        
                console.log(monument);
                
                mapboxgl.accessToken =
                    'pk.eyJ1IjoibHVuYW1hcGJveCIsImEiOiJja2Q0NmJkN3MwM2M5MnRzODM5ZmF5eXo4In0.G9DlfUlW1KnpUDV7Ou93ww';
                var map = new mapboxgl.Map({
                    container: 'map',
                    style: 'mapbox://styles/mapbox/streets-v11',
                    center: monument,
                    zoom: 4
                });

                // create the popup
                var popup = new mapboxgl.Popup({
                    offset: 25
                }).setText(
                    this.getAttribute("data-country")
                );

                // create DOM element for the marker
                var el = document.createElement('div');
                el.id = 'marker';

                // create the marker
                new mapboxgl.Marker(el)
                    .setLngLat(monument)
                    .setPopup(popup) // sets a popup on this marker
                    .addTo(map);
            })

            $('.flag').each(function() {
                var result = $(this).attr('src').replace(" ", "-");
                $(this).attr('src', result);
            });

            $('#q').keyup(function () {
                var filter = this.value.toLowerCase().trim();  // no need to call jQuery here
                $('.data').each(function() {
                    var _this = $(this);
                    var title = this.getAttribute("data-country").toLowerCase().trim();
                    
                    if (title.indexOf(filter) < 0) {
                        _this.hide();
                    }
                    if(filter == 0){
                        _this.show();
                    }
                });
            });
        })
    </script>
@endsection
